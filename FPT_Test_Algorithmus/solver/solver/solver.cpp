#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <vector>
#include <algorithm>
#include <cmath>
//#include <graaflib/algorithm/topological_sorting/dfs_topological_sorting.h>
//#include <graaflib/graph.h> // graph library because im not implementing topo sort

using namespace std;


//crossing numbers



void calculate_Augmentations(int num_verts_bottom,int top_vert_num, std::vector<std::vector<char>> adjMat, std::vector<int>& l_most, std::vector<int>& r_most, std::vector<std::vector<int>>& r, std::vector<std::vector<int>>& p) {

	//neighbors

	for (int i = 1; i <= num_verts_bottom; i++) {//check if not <=

		for (int j = 1; j <= top_vert_num; j++) {

			//calculate rij sum of edges to the right of j
			int sum = 0;
			for (int r = j + 1; r <= top_vert_num; r++) {
				if (adjMat[i][r] == 1) {
					sum++;
				}
			}
			r[i][j] = sum;

			//calculate pij the first neighbor of vert i that is to the right of j

			p[i][j] = top_vert_num + 1;//first top vert
			for (int r = j + 1; r <= top_vert_num; r++) {
				if (adjMat[i][r] == 1) {
					p[i][j] = r;
					break;
				}
			}
		}

		//calculate rightmost neighbor of i
		int right_most = 0;
		for (int r = 1; r <= top_vert_num; r++) {//only want to look at top verts these begin after bottomvert num
			if (adjMat[i][r] == 1) {
				right_most = r;
			}
		}
		
		r_most[i] = right_most;

		//calculate leftmost neighbor of i
		int left_most = 0;
		for (int r = 1; r <= top_vert_num; r++) {
			if (adjMat[i][r] == 1) {
				left_most = r;
				break;
			}
		}

		l_most[i] = left_most;

	}
}

//Step 3 rekursion

struct node {
	std::vector<std::vector<char>> D;
	int B = 0;
	struct node* left_node = NULL;
	struct node* right_node = NULL;
};

void addEdgeAndEnsureTransitivity(std::vector<std::vector<char>>& D, int v, int w) {

	// assume D is already transitive

	//every one smaller than v is also smaller than w
	for (int j = 0; j < D.size(); j++) {
		if (D[j][v] == 1) {
			D[j][w] = 1;
		}
		if (D[w][j] == 1) {
			D[v][j] = 1;
		}
	}

}

node CreateNewNode(int num_verts_bottom, int v, int w, int B, std::vector<std::vector<char>>& D, std::vector<std::vector<int>> crossingMat) {
	std::vector<std::vector<char>> D1(D);

	//Creating the new D matrix with the new edge so we need to also add the edges that need to be added because of transitive closure
	// D[a][b] = 1 means that a > b (in the order)
	// so if a > b then a > all nums before b in the sequence
	// => D[a][0...b-1] = 1

	D1[v][w] = 1;

	

	/*
	for (int i = 1; i < v; i++) {
		D1[v][i] = 1;// adding the transitive closure    check this... 
		sum = sum + crossingMat[v][i]; //sum for the budget
	}*/

	addEdgeAndEnsureTransitivity(D1, v, w);

	//sum of cw edges added through transitivity
	//sum of directed edges added by transitive closure with cpq != cqp
	
	int sum = 0;
	
	for (int j = 0; j < D.size(); j++) {
		if (D[j][v] == 1) {
			if (crossingMat[j][w] != crossingMat[w][j]) {
				sum += crossingMat[j][w];
			}
		}
	}


	//calculate the budget...

	int B1 = B - crossingMat[v][w] - sum;

	node n;
	n.D = D1;
	n.B = B;
	return n;
}


template <typename T> void showmatrix(std::vector<std::vector<T>> m) {
    
    //adjust number of spaces so that the table doesnt look wierd because the length of the numbers
    int side_length = m.size() - 1;
    
    int max_double_num_char_len = (int) log10(side_length*2) + 1;
    
	for(int k = 0; k < max_double_num_char_len + 1; k++){
		    std::cout << " ";
	}
		
	for (int i = 1; i < m[0].size(); i++) {
		std::cout << i << " ";
		for(int k = 0; k < max_double_num_char_len - ((int) log10(i) + 1); k++){
		    std::cout << " ";
		}
	}
	
	
	std::cout << " A\n";
	for (int i = 1; i < m.size(); i++) {
		
		int row_num = i + m[0].size() - 1;
		std::cout << row_num << " ";
		for(int k = 0; k < max_double_num_char_len - ((int) log10(row_num) + 1); k++){
		    std::cout << " ";
		}
		
		for (int j = 1; j < m[0].size(); j++) {
			std::cout << std::to_string(m[i][j]) << " ";
			for(int k = 0; k < max_double_num_char_len - ((int) log10(m[i][j]+1) + 1); k++){
		        std::cout << " ";
		    }
		}
		std::cout << "\n";
	}
	std::cout << "B\n";
}


vector<int> ans;

struct toposort {
	int n = 0; // number of vertices

	vector<vector<int>> adj; // adjacency list of graph


	toposort(vector<vector<char>> adjMat) {
		for (int i = 0; i < adjMat.size(); i++) {
			//What is smaller that v??
			vector<int> v;
			adj.push_back(v);
			for (int j = 0; j < adjMat.size(); j++) {
				if (adjMat[i][j] == 1) {
					adj[i].push_back(j);
				}
			}
		}

		n = adjMat.size();

		topological_sort();

	}


	vector<bool> visited;
	vector<int> ans;

	void dfs(int v) {
		visited[v] = true;
		for (int u : adj[v]) {
			if (!visited[u])
				dfs(u);
		}
		ans.push_back(v);
	}

	void topological_sort() {
		visited.assign(n, false);
		ans.clear();
		for (int i = 0; i < n; ++i) {
			if (!visited[i]) {
				dfs(i);
			}
		}
		std::reverse(ans.begin(), ans.end());
	}

	void print() {
		for (int i = 1; i < ans.size(); i++) {
			std::cout << ans[i] << " ";
		}
		std::cout << "\n";
	}

};

node build_tree_and_search(int max_crossings_k, node N, int num_verts_bottom, std::vector<std::vector<int>> crossingMat) {

	//start with Node N
	//N.D;
	//N.B;

	// check the B
	//check if it ist Leaf

	if (N.B < 0) {
		std::printf("Error ?? how??\n");
	}

	//if leaf do topo sort

	//solution is topo sort of D

	//calc new B
	//topo orde is current best order
	//return best order;

	// how does the best order get to the output???

	//else:

	// pick the pair

	//choosing a pair where cwv != cvw and it is not in D) or just not in C?

	int pair_v = 0;
	int pair_w = 0;

	bool found_pair = false;

	for (int v = 1; v <= num_verts_bottom; v++) {
		for (int w = v; w <= num_verts_bottom; w++) {
			if (crossingMat[v][w] != crossingMat[w][v] && N.D[w][v] == 0 && N.D[v][w] == 0 && N.D[w][v] != -1 && N.D[v][w] != -1) {
				pair_v = v;
				pair_w = w;
				found_pair = true;
				std::cout << "Found pair " << pair_v << " and " << pair_w << "\n";
				//to break out of the outer for loop
				v = num_verts_bottom + 1;
				break;
			}
		}
	}

	std::vector<std::vector<char>> D1(N.D); //copy D0

	//create the two Children 

	//rekursion

	node A = CreateNewNode(num_verts_bottom, pair_v, pair_w, N.B, N.D, crossingMat);

	node B = CreateNewNode(num_verts_bottom, pair_w, pair_v, N.B, N.D, crossingMat);

	if (A.B < 0 && B.B < 0) {// getting data if its a leaf all other pairs of vw need to have a negative budget or vw is part of  C
		//is bad so try again with another pair
		// mark vw as bad
		N.D[pair_w][pair_v] = -1;
		N.D[pair_v][pair_w] = -1;
		build_tree_and_search(max_crossings_k, N, num_verts_bottom, crossingMat);
	}

	if (found_pair == false) { //|| N.D[pair_w][pair_v] == N.D[pair_v][pair_w]) {// check if is a leaf

		//topo sort of D
		

		toposort top(N.D);


		// std::printf("Min num crossings found %i\n",max_crossings_k - N.B);

		// showmatrix(N.D);

		// std::cout << "\n";

		// top.print();

		

		return N;
	}

	node max_A;
	max_A.B = -1;
	node max_B;
	max_B.B = -1;
	if (A.B > 0) {
		max_A = build_tree_and_search(max_crossings_k, A, num_verts_bottom, crossingMat);
	}

	if (B.B > 0) {
		max_B = build_tree_and_search(max_crossings_k, B, num_verts_bottom, crossingMat);
	}
	if(max_A.B > max_B.B) {
		return max_A;
	}
	return max_B;	
}

void calculate_crossing_matrix(int num_bottom_verts, std::vector<int>& l_most, std::vector<int>& r_most, std::vector<std::vector<int>>& r, std::vector<std::vector<int>>& p, std::vector<std::vector<int>>& c) {
	
	//this algorithm is from the paper and seems to be kind of slow .....

	for (int v = 1; v <= num_bottom_verts; v++) {// <= or < ...???
		for (int w = 1; w <= num_bottom_verts; w++) {
			if (v != w) {
				c[v][w] = 0;
				int w1 = l_most[w];
				while (w1 <= r_most[w] && w1 < r_most[v]){
					c[v][w] = c[v][w] + r[v][w1];
					w1 = p[w][w1];
				}
			}
		}
	}
}

int main(int argc, char* argv[]) {
	if(argc != 2) {
		std::cout << "Usage: ./solver.out [testfile]\n";
		return -1;
	}
	std::printf("opened file %s \n", argv[1]);
	string line;
	ifstream myfile(argv[1]);

	/* Input format:
	- ocr: problem descriptor
	- n0: number of vertices in fixed bipartition A
	- n1: number of vertices in free bipartition
	- m: number of edges
	*/

	string type;
	int num_verts_top = 0;
	int num_verts_bottom = 0;
	int num_edges_to_follow = 0;

	vector<int> A_verts;

	vector<int> bottom_verts;

	if (myfile.is_open())
	{
		getline(myfile, line);

		if (4 == sscanf(line.c_str(), "p %s %i %i %i", &type[0], &num_verts_top, &num_verts_bottom, &num_edges_to_follow)) {
			std::printf("type %s got %i top verts %i bottom verts %i edges \n", type.c_str(), num_verts_top, num_verts_bottom, num_edges_to_follow);
		}
		std::printf("%s\n", line.c_str());

		//adjacency matrix its not a standard one though
		std::vector<std::vector<char>> adjMat;

		//ists a num_verts_top by num_verts_bottom matrix

		// adjMatrix[index of bottom vert][index of top vert] = 1 if adjacent

		for (int i = 0; i < num_verts_bottom+1; i++) {
			std::vector<char> row;
			row.resize(num_verts_top+1);
			adjMat.push_back(row);
		}

		

		std::vector<std::vector<int>> crossingMat;

		for (int i = 0; i < num_verts_bottom + 1; i++) {
			std::vector<int> row;
			row.resize(num_verts_bottom + 1);
			crossingMat.push_back(row);
		}

		//lw
		//leftmost neighbor 
		std::vector<int> l_most;

		//rw
		//rightmost neighbor
		std::vector<int> r_most;

		//rij number of the neighbors of i to the right of j
		std::vector<std::vector<int>> r;

		//pij first neighbor of vertex i that is to the right of j
		std::vector<std::vector<int>> p;

		//init the arrays
		l_most.resize(num_verts_bottom+1);
		r_most.resize(num_verts_bottom+1);

		for (int i = 0; i < num_verts_bottom+1; i++) {
			std::vector<int> row;
			row.resize(num_verts_top+1);
			r.push_back(row);
			std::vector<int> row1;
			row1.resize(num_verts_top+1);
			p.push_back(row1);
		}


		int num_edges = 0;

		// read file
		while (getline(myfile, line))
		{
			std::cout << line << '\n';
			int a, b = 0;
			if (std::sscanf(line.c_str(), "%i %i", &a, &b) != 2) {
				std::printf("ERROR bad data???\n");
				return -1;
			}
			A_verts.push_back(a);
			bottom_verts.push_back(b);

			num_edges++;

			//b = n0+1 bis n0 + n1
			//get the vertices in order...
			adjMat[b-num_verts_top][a] = 1;
		
			//b-num_verts_top-1
		}
		myfile.close();

		//Step 0: Calculate crossing matrix

		std::cout << "Adjacency matrix \n";

		showmatrix(adjMat);

		std::printf("calculating Augmentend matrix\n");

		calculate_Augmentations(num_verts_bottom, num_verts_top, adjMat, l_most, r_most, r, p);
		std::printf("r matrix\n");
		showmatrix(r);
		std::printf("p matrix\n");
		showmatrix(p);


		std::printf("calculating Crossing matrix\n");

		calculate_crossing_matrix(num_verts_bottom, l_most, r_most, r, p, crossingMat);

		showmatrix(crossingMat);

		//Step 1: Check for extreme values

		std::printf("checking for extreme values\n");

		int sum_of_max_crossings = 0;
		int sum_of_min_crossings = 0;
		for (int v = 1; v <= num_verts_bottom; v++) {
			for (int w = v; w <= num_verts_bottom; w++) {
				sum_of_max_crossings = sum_of_max_crossings + std::max(crossingMat[v][w], crossingMat[w][v]);
				sum_of_min_crossings = sum_of_min_crossings + std::min(crossingMat[v][w], crossingMat[w][v]);
			}
		}

		int max_crossings_k = std::ceil(1.47 * sum_of_min_crossings);
		std::cout << "k = " << max_crossings_k << "\n";

		//verbesserung nach dem paper k befindet sich in dem Range der Anzahl der Erlaubten Crossings
		if (max_crossings_k < sum_of_min_crossings) {
			std::printf("k ist not maximum ERROR\n");
			return 0;
		}
		
		if (max_crossings_k >= sum_of_max_crossings) {
			std::cout << "High k, set k = " << sum_of_max_crossings << " (sum_of_max_crossings)\n";
			max_crossings_k = sum_of_max_crossings;
		}

		//Step 2: Prepare C, D0 and B0

		//computing C group of all (v,w) where cvw = cwv

		std::printf("computing C group\n");

		std::vector<std::vector<char>> C;

		//init the array
		for (int i = 0; i < num_verts_bottom + 1; i++) {
			std::vector<char> row;
			row.resize(num_verts_bottom + 1);
			C.push_back(row);
		}

		for (int v = 1; v <= num_verts_bottom; v++) {
			for (int w = 1; w <= num_verts_bottom; w++) {
				if (crossingMat[v][w] == crossingMat[w][v]) {
					C[v][w] = 1;
				}
			}
		}

		showmatrix(C);

		//making D0 a directed acyclic graph.... of the "<" operator of the sequence of the bottom verts

		std::printf("creating DAG D0\n");

		std::vector<std::vector<char>> D0;

		for (int i = 0; i < num_verts_bottom + 1; i++) {
			std::vector<char> row;
			row.resize(num_verts_bottom + 1);
			D0.push_back(row);
		}

		// THERE MAY BE AN ERROR HERE /////////////////////////////////////////////

		for (int v = 1; v <= num_verts_bottom; v++) {
			for (int w = 1; w <= num_verts_bottom; w++) {
				if (crossingMat[v][w] == 0 && crossingMat[w][v] != 0) {
					
					D0[v][w] = 1;

					addEdgeAndEnsureTransitivity(D0, v, w);

					//transitivity (check again)
					/*for (int i = 1; i <= num_verts_bottom; i++) {
						if(D0[w][i] == 1) {
							D0[v][i] = 1;
						}
					}*/
				}
			}
		}

		// D0 is transitively closed and represents the < operator...
		// so if a < b and b < c then a < c is also in D

		showmatrix(D0);

		//Initial Budget B0

		std::printf("calculating Budget B0\n");

		int sum_of_crossing_values_of_edges_in_C = 0;

		for (int v = 1; v <= num_verts_bottom; v++) {
			for (int w = 1; w <= num_verts_bottom; w++) {
				if (C[v][w] == 1) {
					sum_of_crossing_values_of_edges_in_C = sum_of_crossing_values_of_edges_in_C + crossingMat[v][w];
				}
			}
		}

		sum_of_crossing_values_of_edges_in_C /= 2;

		int B0 = max_crossings_k - sum_of_crossing_values_of_edges_in_C;

		std::cout << "Startbudget: " << B0 << "\n";

		//Step 3

		std::printf("Creating Tree and Searching at the same time\n");

		node root = { D0, B0 };

		std::vector<std::vector<char>> D(D0);

		// need to do some recursion....

		node opt = build_tree_and_search(max_crossings_k, root, num_verts_bottom, crossingMat);
		std::cout << "Restbudget optimale Lösung: " << opt.B << "\nD-Matrix: \n";
		showmatrix(opt.D);

		toposort top(opt.D);

		std::cout << "\n";

		top.print();

		//write to file
		string filename = "solution";
		//sscanf(argv[1], "%i.gr", &filename[0]);

		ofstream myfile(filename + ".sol");
		if (myfile.is_open())
		{

			for (auto i : top.ans) {
			    if (i > 0){
			        myfile << i + num_verts_top << "\n";
			    }
			}
			myfile.close();
		}
		else std::cout << "Unable to open file";
	}
	else {
		std::cout << "Unable to open file";
	}

	return 0;
}