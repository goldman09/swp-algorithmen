# SWP Algorithmen: PACE 2024

## Description
This project deals with the Exact Track of the PACE Challenge 2024 as part of the software project "Anwendungen von Algorithmen S24" at the Freie Universität Berlin. <br> <br>
PACE Challenge: https://pacechallenge.org/2024/ <br>
Test Sets: https://github.com/PhKindermann/Pace2024-Testsets/tree/main/ <br>

## Submission

## How to run solver
``g++ solver.cpp -o solver.out`` <br>
``./solver.out <Testdatei>``
## How to run testscripts
### Tiny Test Set
Start running **test_tinyset.sh** for the solver with all test graphs in tiny_test_sets in the Linux shell. <br>
The solutions are in the **test_solutions_tiny** folder, the verification is in the **output_tinyset.log** file. The duration of execution is in the **time_tinyset.log** file. <br>
``sudo ./test_tinyset.sh`` <br> <br>
Start running **test_mediumset.sh** for the solver with all test graphs in tiny_test_sets in the Linux shell. Change the **TIMEOUT** variable (in seconds) to stop the calculation for each graph after this time. <br>
The solutions are in the **test_solutions_medium** folder, the verification is in the **output_mediumset.log** file. The duration of execution is in the **time_mediumset.log** file. <br>
``sudo ./test_mediumset.sh`` <br> <br>


## How to run tester, verifier and visualizer
### Install visualizer
``pip install -r visualizer/requirements.txt``
### Install tester and verifier
``pip install pace2024-verifier``
### Run visualizer
``python ./visualizer/visualizer.py <path_to_graph.gr> <path_to_solution_sol>`` <br> <br>
Example:<br>
``python ./visualizer/visualizer.py test_sets/tiny_test_set/instances/complete_4_5.gr test_sets/tiny_test_set/solutions/complete_4_5.sol``
Start running test.sh for 

## Requirements and Dependencies


## License
The project is subject to the GNU GENERAL PUBLIC LICENSE Version 3. The GNU GENERAL PUBLIC LICENSE Version 3 is a strong non-permissive copyleft license. For more details see the license file.
