#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <vector>
#include <algorithm>
#include <cmath>
#include <stack>
#include <unistd.h>
#include <tuple>

using namespace std;

int current_best_budget = 0;
const bool verbose = false;
const bool optimize = true;
const bool compare_to_suggested_solution = false;


void calculate_augmentations(int num_verts_bottom, int top_vert_num, vector<vector<int>> adj_mat, vector<int>& l_most, vector<int>& r_most, vector<vector<int>>& r, vector<vector<int>>& p) {

	for (int i = 1; i <= num_verts_bottom; i++) {
		for (int j = 1; j <= top_vert_num; j++) {
			int sum = 0;
			for (int r = j + 1; r <= top_vert_num; r++) {
				if (adj_mat[i][r] == 1) {
					sum++;
				}
			}
			r[i][j] = sum;

			p[i][j] = top_vert_num + 1;
			for (int r = j + 1; r <= top_vert_num; r++) {
				if (adj_mat[i][r] == 1) {
					p[i][j] = r;
					break;
				}
			}
		}

		int right_most = 0;
		for (int r = 1; r <= top_vert_num; r++) {
			if (adj_mat[i][r] == 1) {
				right_most = r;
			}
		}
		
		r_most[i] = right_most;

		int left_most = top_vert_num + 1;
		for (int r = 1; r <= top_vert_num; r++) {
			if (adj_mat[i][r] == 1) {
				left_most = r;
				break;
			}
		}

		l_most[i] = left_most;

	}
}

//Step 3: Recursion

struct node {
	vector<vector<int>> D;
	int B = 0;
	struct node* left_node = NULL;
	struct node* right_node = NULL;
	int recursion_depth = 0;
	int min_remaining_cost = 0;
};

int num_digits(int i) {
	if(i == 0) {
		return 1;
	}
	return (int) (log10(i) + 1);
}

template <typename T> void show_matrix(vector<vector<T>> m, bool only_verts_bottom = false, int num_verts_top = 0) {

	int num_rows = m.size() - 1;
	int num_columns = num_verts_top;

	int cell_width;
	if(only_verts_bottom) {
		cell_width = num_digits(2 * num_columns) + 2;
	} else {
		cell_width = num_digits(num_rows * num_columns) + 2;
	}

	for(int k = 0; k < cell_width; k++) {
		cout << " ";
	}

	for (int i = 1; i < m[0].size(); i++) {
		if (only_verts_bottom) {
			cout << i + num_verts_top;
			for(int k = 0; k < cell_width - num_digits(i + num_verts_top); k++) {
				cout << " ";
			}
		}
		else {
			cout << i;
			for(int k = 0; k < cell_width - num_digits(i); k++) {
		    	cout << " ";
			}
		}
	}

	if (only_verts_bottom) {
		cout << " B\n";
	}
	else {
		cout << " A\n";
	}

	for (int i = 1; i < m.size(); i++) {

		int row_num;
		if (only_verts_bottom) {
			row_num = i + num_verts_top;
		}
		else {
			row_num = i + num_columns;
		}
		cout << row_num;
		for(int k = 0; k < cell_width - num_digits(row_num); k++){
		    cout << " ";
		}

		for (int j = 1; j < m[0].size(); j++) {
			cout << to_string(m[i][j]);
			for(int k = 0; k < cell_width - num_digits(m[i][j]); k++){
		        cout << " ";
		    }
		}
		cout << "\n";
	}
	cout << "B\n";
}

int calculate_min_remaining_cost(vector<vector<int>>& D, vector<vector<int>> crossing_mat) {
	int cost = 0;
	for(int i = 1; i < D.size(); i++) {
		for(int j = i; j < D.size(); j++) {
			if(D[i][j] == 0 && D[j][i] == 0) {
				if(crossing_mat[i][j] != crossing_mat[j][i]) {
					cost += min(crossing_mat[i][j], crossing_mat[j][i]);
				}
			}
		}
	}
	return cost;
}

tuple<int, int> add_edge_and_ensure_transitivity(vector<vector<int>>& D, vector<vector<int>> crossing_mat, int v, int w) {

	D[v][w] = 1;
	int sum_budget_cost = 0;
	int sum_remaining_cost = 0;

	vector<int> can_reach_v;
	vector<int> reachable_by_v;

	for(int i = 1; i < D.size(); i++) {
		if(D[i][v] == 1) {
			if(D[i][w] == 0) {
				D[i][w] = 1;

				if(crossing_mat[i][w] != crossing_mat[w][i]) {
					sum_budget_cost += crossing_mat[i][w];
					sum_remaining_cost += min(crossing_mat[i][w], crossing_mat[w][i]);
				}
			}
			can_reach_v.push_back(i);
		}
		if(D[w][i] == 1) {
			if(D[v][i] == 0) {
				D[v][i] = 1;

				if(crossing_mat[v][i] != crossing_mat[i][v]) {
					sum_budget_cost += crossing_mat[v][i];
					sum_remaining_cost += min(crossing_mat[v][i], crossing_mat[i][v]);
				}
			}
			reachable_by_v.push_back(i);
		}
	}

	for (int i = 0; i < can_reach_v.size(); i++) {
		for(int j = 0; j < reachable_by_v.size(); j++) {
			if(D[can_reach_v[i]][reachable_by_v[j]] == 0) {
				D[can_reach_v[i]][reachable_by_v[j]] = 1;

				if(crossing_mat[can_reach_v[i]][reachable_by_v[j]] != crossing_mat[reachable_by_v[j]][can_reach_v[i]]) {
					sum_budget_cost += crossing_mat[can_reach_v[i]][reachable_by_v[j]];
					sum_remaining_cost += min(crossing_mat[can_reach_v[i]][reachable_by_v[j]], crossing_mat[reachable_by_v[j]][can_reach_v[i]]);
				}	
			}
		}
	}
	return make_tuple(sum_budget_cost, sum_remaining_cost);
}

node create_new_node(int num_verts_bottom, int v, int w, int B, vector<vector<int>>& D, vector<vector<int>> crossing_mat, int recursion_depth, int min_remaining_cost) {
	vector<vector<int>> D1(D);
	int sum_budget_cost, sum_remaining_cost;
	tie(sum_budget_cost, sum_remaining_cost) = add_edge_and_ensure_transitivity(D1, crossing_mat, v, w);

	int B1 = B - crossing_mat[v][w] - sum_budget_cost;

	node n;
	n.D = D1;
	n.B = B1;
	n.recursion_depth = recursion_depth + 1;
	n.min_remaining_cost = min_remaining_cost - min(crossing_mat[v][w], crossing_mat[w][v]) - sum_remaining_cost;
	int expected_remaining_cost = calculate_min_remaining_cost(n.D, crossing_mat);
	if(verbose) {
		cout << "Budget for new pair " << v << "," << w << ": " << B1 << ", recursion depth " << n.recursion_depth << ", minimum remaining cost " << n.min_remaining_cost << "\n";
	}
	return n;
}

struct toposort {
	int n = 0;

	vector<vector<int>> adj;


	toposort(vector<vector<int>> adj_mat) {
		for (int i = 0; i < adj_mat.size(); i++) {
			vector<int> v;
			adj.push_back(v);
			for (int j = 0; j < adj_mat.size(); j++) {
				if (adj_mat[i][j] == 1) {
					adj[i].push_back(j);
				}
			}
		}

		n = adj_mat.size();

		topological_sort();
	}


	vector<int> visited;
	vector<int> ans;

	void dfs(int v) {
		visited[v] = true;
		for (int u : adj[v]) {
			if (!visited[u])
				dfs(u);
		}
		ans.push_back(v);
	}

	void topological_sort() {
		visited.assign(n, false);
		ans.clear();
		for (int i = 0; i < n; ++i) {
			if (!visited[i]) {
				dfs(i);
			}
		}
		reverse(ans.begin(), ans.end());
	}

	void print(int num_verts_top) {
		for (int i = 0; i < ans.size()-1; i++) {
			cout << ans[i] + num_verts_top << " ";
		}
		cout << "\n";
	}

};

node build_tree_and_search(int max_crossings_k, node n, int num_verts_bottom, vector<vector<int>> crossing_mat) {

	if(n.B < current_best_budget + (optimize ? n.min_remaining_cost: 0)) {
		if(verbose) {
			cout << "Better solution (budget " << current_best_budget << " + minimum remaining cost " << n.min_remaining_cost << " > " << n.B << ") already exists, pruning branch\n";
		}
		n.B = -1;
		return n;
	}

	int pair_v = 0;
	int pair_w = 0;

	bool found_pair = false;

	for (int v = 1; v <= num_verts_bottom; v++) {
		for (int w = v+1; w <= num_verts_bottom; w++) {
			if (crossing_mat[v][w] != crossing_mat[w][v] && n.D[w][v] == 0 && n.D[v][w] == 0 && n.D[w][v] != -1 && n.D[v][w] != -1) {
				pair_v = v;
				pair_w = w;
				found_pair = true;
				if(verbose) {
					cout << "Found pair " << pair_v << " and " << pair_w << "\n";
				}
				v = num_verts_bottom + 1;
				break;
			}
		}
	}

	if (found_pair == false) {
		
		toposort top(n.D);
		if(n.B > current_best_budget) {
			current_best_budget = n.B;
			if(verbose) {
				cout << "New best budget: " << current_best_budget << "\n";
			}	
		}

		return n;
	}

	vector<vector<int>> D1(n.D);

	node a = create_new_node(num_verts_bottom, pair_v, pair_w, n.B, n.D, crossing_mat, n.recursion_depth, n.min_remaining_cost);

	node b = create_new_node(num_verts_bottom, pair_w, pair_v, n.B, n.D, crossing_mat, n.recursion_depth, n.min_remaining_cost);

	if (a.B < 0 && b.B < 0) {
		n.D[pair_w][pair_v] = -1;
		n.D[pair_v][pair_w] = -1;
		build_tree_and_search(max_crossings_k, n, num_verts_bottom, crossing_mat);
	}

	node max_a;
	max_a.B = -1;
	node max_b;
	max_b.B = -1;
	if (a.B > b.B) {
		if (a.B > 0) {
			max_a = build_tree_and_search(max_crossings_k, a, num_verts_bottom, crossing_mat);
		}

		if (b.B > 0) {
			max_b = build_tree_and_search(max_crossings_k, b, num_verts_bottom, crossing_mat);
		}
	} else {
		if (b.B > 0) {
			max_b = build_tree_and_search(max_crossings_k, b, num_verts_bottom, crossing_mat);
		}

		if (a.B > 0) {
			max_a = build_tree_and_search(max_crossings_k, a, num_verts_bottom, crossing_mat);
		}
	}
	if(max_a.B > max_b.B) {
		return max_a;
	}
	return max_b;	
}

void calculate_crossing_matrix(int num_bottom_verts, vector<int>& l_most, vector<int>& r_most, vector<vector<int>>& r, vector<vector<int>>& p, vector<vector<int>>& c) {

	for (int v = 1; v <= num_bottom_verts; v++) {
		for (int w = 1; w <= num_bottom_verts; w++) {
			if (v != w) {
				c[v][w] = 0;
				int w1 = l_most[w];
				while (w1 <= r_most[w] && w1 < r_most[v]){
					c[v][w] = c[v][w] + r[v][w1];
					w1 = p[w][w1];
				}
			}
		}
	}
}

vector<vector<int>> compute_one_layer_cut_vertices(int num_verts_top, int num_verts_bottom, vector<int> l_most, vector<int> r_most) {
	vector<vector<int>> one_layer_partitions;
	bool node_fits;
	for (size_t i = 1; i <= num_verts_top; i++) {
		node_fits = true;
		vector<int> partition_left;
		vector<int> partition_middle;
		vector<int> partition_right;
		for (size_t j = 1; j <= num_verts_bottom; j++) {
			if(r_most[j] <= i && l_most[j] >= i) {
				partition_middle.push_back(j);
			} else if(r_most[j] <= i) {
				partition_left.push_back(j);
			} else if(l_most[j] >= i) {
				partition_right.push_back(j);
			} else {
				node_fits = false;
				break;
			}
		}
		bool left_nonempty = partition_left.size() + partition_middle.size() > 0;
		bool right_nonempty = partition_right.size() + partition_middle.size() > 0;
		if(node_fits && left_nonempty && right_nonempty) {
			cout << "Node " << i << " is 1-layer cut vertex\n";
			one_layer_partitions.push_back(partition_left);
			one_layer_partitions.push_back(partition_middle);
			one_layer_partitions.push_back(partition_right);
		}
	}
	
	return one_layer_partitions;
}

int calculate_num_crossings(vector<int> permutation, vector<vector<int>> crossing_mat) {
	int num_crossings = 0;
	for(int i = 0; i < permutation.size(); i++) {
		for(int j = i; j < permutation.size(); j++) {
			num_crossings += crossing_mat[permutation[i]][permutation[j]];
		}
	}
	return num_crossings;
}

int main(int argc, char* argv[]) {
	if(argc != 2) {
		cout << "Usage: ./solver.out [testfile]\n";
		return -1;
	}
	string line;
	ifstream myfile(argv[1]);

	/* Input format:
	- ocr: problem descriptor
	- n0: number of vertices in fixed bipartition A
	- n1: number of vertices in free bipartition
	- m: number of edges
	*/

	string type;
	int num_verts_top = 0;
	int num_verts_bottom = 0;
	int num_edges_to_follow = 0;

	vector<int> A_verts;

	vector<int> bottom_verts;

	if (myfile.is_open()) {
		cout << "Opened file " << argv[1] << "\n";
		getline(myfile, line);

		if (4 == sscanf(line.c_str(), "p %s %i %i %i", &type[0], &num_verts_top, &num_verts_bottom, &num_edges_to_follow)) {
			printf("type %s got %i top verts %i bottom verts %i edges \n", type.c_str(), num_verts_top, num_verts_bottom, num_edges_to_follow);
		}
		printf("%s\n", line.c_str());

		vector<vector<int>> adj_mat;

		for (int i = 0; i < num_verts_bottom+1; i++) {
			vector<int> row;
			row.resize(num_verts_top+1);
			adj_mat.push_back(row);
		}

		

		vector<vector<int>> crossing_mat;

		for (int i = 0; i < num_verts_bottom + 1; i++) {
			vector<int> row;
			row.resize(num_verts_bottom + 1);
			crossing_mat.push_back(row);
		}

		//leftmost neighbor lw
		vector<int> l_most;

		//rightmost neighbor rw
		vector<int> r_most;

		//rij number of the neighbors of i to the right of j
		vector<vector<int>> r;

		//pij first neighbor of vertex i that is to the right of j
		vector<vector<int>> p;

		//init the arrays
		l_most.resize(num_verts_bottom+1);
		r_most.resize(num_verts_bottom+1);

		for (int i = 0; i < num_verts_bottom+1; i++) {
			vector<int> row;
			row.resize(num_verts_top+1);
			r.push_back(row);
			vector<int> row1;
			row1.resize(num_verts_top+1);
			p.push_back(row1);
		}


		int num_edges = 0;

		// read file
		while (getline(myfile, line))
		{
			cout << line << '\n';
			int a, b = 0;
			if (sscanf(line.c_str(), "%i %i", &a, &b) != 2) {
				printf("ERROR bad data???\n");
				return -1;
			}
			A_verts.push_back(a);
			bottom_verts.push_back(b);

			num_edges++;

			//b = n0+1 bis n0 + n1
			//get the vertices in order...
			adj_mat[b-num_verts_top][a] = 1;
		
			//b-num_verts_top-1
		}
		myfile.close();

		//Step 0: Calculate crossing matrix

		cout << "Adjacency matrix \n";

		if(verbose) {
			show_matrix(adj_mat, true, num_verts_top);
		}

		printf("calculating Augmentend matrix\n");

		calculate_augmentations(num_verts_bottom, num_verts_top, adj_mat, l_most, r_most, r, p);
		if(verbose) {
			printf("r matrix\n");
			show_matrix(r);
			printf("p matrix\n");
			show_matrix(p);
		}

		/* Compute 1-layer cut vertices */
		vector<vector<int>> one_layer_partitions = compute_one_layer_cut_vertices(num_verts_top, num_verts_bottom, l_most, r_most);
		cout << "Found the following partitions for 1-layer cut vertices:\n";
		for(size_t i = 0; i < one_layer_partitions.size(); i = i+3) {
			cout << "* Partition " << (i+3)/3 << ": ";
			for(size_t j = 0; j < one_layer_partitions[i].size(); j++) {
				cout << one_layer_partitions[i][j] + num_verts_top << " ";
			}
			cout << "-- ";
			for(size_t j = 0; j < one_layer_partitions[i+1].size(); j++) {
				cout << one_layer_partitions[i+1][j] + num_verts_top << " ";
			}
			cout << "-- ";
			for(size_t j = 0; j < one_layer_partitions[i+2].size(); j++) {
				cout << one_layer_partitions[i+2][j] + num_verts_top << " ";
			}
			cout << "\n";
		}
		cout << "\n";

		printf("Calculating crossing matrix\n");

		calculate_crossing_matrix(num_verts_bottom, l_most, r_most, r, p, crossing_mat);
		if(verbose) {
			show_matrix(crossing_mat, true, num_verts_top);
		}

		//Step 1: Check for extreme values

		printf("Checking for extreme values\n");

		int sum_of_max_crossings = 0;
		int sum_of_min_crossings = 0;
		for (int v = 1; v <= num_verts_bottom; v++) {
			for (int w = v; w <= num_verts_bottom; w++) {
				sum_of_max_crossings = sum_of_max_crossings + max(crossing_mat[v][w], crossing_mat[w][v]);
				sum_of_min_crossings = sum_of_min_crossings + min(crossing_mat[v][w], crossing_mat[w][v]);
			}
		}
		printf("MAX: %d\n", sum_of_max_crossings);
		printf("MIN: %d\n", sum_of_min_crossings);

		int max_crossings_k = ceil(1.47 * sum_of_min_crossings);

		if (max_crossings_k < sum_of_min_crossings) {
			printf("k ist not maximum ERROR\n");
			return 0;
		}
		
		if (max_crossings_k >= sum_of_max_crossings) {
		    cout << "High k, set k = " << sum_of_max_crossings << " (sum_of_max_crossings)\n";
			max_crossings_k = sum_of_max_crossings;
		}

		cout << "k = " << max_crossings_k << "\n";

		if (sum_of_min_crossings == sum_of_max_crossings)
		{
				
			vector<vector<int>> D;

			for (int i = 0; i < num_verts_bottom + 1; i++) {
				vector<int> row;
				row.resize(num_verts_bottom + 1);
				D.push_back(row);
			}

			toposort top(D);

			cout << "\n";

			top.print(num_verts_top);

			ofstream myfile("solution.sol");
			if (myfile.is_open())
			{

				for (auto i : top.ans) {
					if (i > 0){
						myfile << i + num_verts_top << "\n";
					}
				}
				myfile.close();
			}
			else cout << "Unable to open file";
			
			return 0;
		}

		

		//Step 2: Prepare C, D0 and B0

		//computing C group of all (v,w) where cvw = cwv

		printf("computing C group\n");

		vector<vector<int>> C;

		//init the array
		for (int i = 0; i < num_verts_bottom + 1; i++) {
			vector<int> row;
			row.resize(num_verts_bottom + 1);
			C.push_back(row);
		}

		for (int v = 1; v <= num_verts_bottom; v++) {
			for (int w = 1; w <= num_verts_bottom; w++) {
				if (crossing_mat[v][w] == crossing_mat[w][v]) {
					C[v][w] = 1;
				}
			}
		}
		if(verbose) {
			show_matrix(C, true, num_verts_top);
		}

		printf("creating DAG D0\n");

		vector<vector<int>> D0;

		for (int i = 0; i < num_verts_bottom + 1; i++) {
			vector<int> row;
			row.resize(num_verts_bottom + 1);
			D0.push_back(row);
		}

		for (int v = 1; v <= num_verts_bottom; v++) {
			for (int w = 1; w <= num_verts_bottom; w++) {
				if (crossing_mat[v][w] == 0 && crossing_mat[w][v] != 0) {

					add_edge_and_ensure_transitivity(D0, crossing_mat, v, w);
				}
			}
		}

		if(verbose) {
			show_matrix(D0, true, num_verts_top);
		}

		//Initial Budget B0

		printf("calculating Budget B0\n");

		int sum_of_crossing_values_of_edges_in_C = 0;

		for (int v = 1; v <= num_verts_bottom; v++) {
			for (int w = 1; w <= num_verts_bottom; w++) {
				if (C[v][w] == 1) {
					sum_of_crossing_values_of_edges_in_C = sum_of_crossing_values_of_edges_in_C + crossing_mat[v][w];
				}
			}
		}

		sum_of_crossing_values_of_edges_in_C /= 2;

		int B0 = max_crossings_k - sum_of_crossing_values_of_edges_in_C;

		cout << "Starting budget: " << B0 << "\n";

		//Step 3

		printf("Creating Tree and Searching at the same time\n");

		node root = { D0, B0 };
		root.min_remaining_cost = sum_of_min_crossings - sum_of_crossing_values_of_edges_in_C;
		cout << "Remaining cost beginning: " << sum_of_min_crossings << " - " << sum_of_crossing_values_of_edges_in_C << " = " << sum_of_min_crossings - sum_of_crossing_values_of_edges_in_C << "\n";

		vector<vector<int>> D(D0);

		node opt = build_tree_and_search(max_crossings_k, root, num_verts_bottom, crossing_mat);
		if(opt.B < 0) {
			cout << "Negative remaining budget, error\n";
			return(-1);
		}
		cout << "Remaining budget for optimal solution: " << opt.B << "\n";

		toposort top(opt.D);

		cout << "\n";
		cout << "Resulting permutation: ";
		top.print(num_verts_top);
		cout << "Number crossings in optimal solution: " << max_crossings_k - opt.B << "\n";
		cout << "Check result: Number crossings is " << calculate_num_crossings(top.ans, crossing_mat) << ".\n";

		if(compare_to_suggested_solution) {
			vector<int> suggested_permutation;
			ifstream solfile("test_sets/medium_test_set/solutions/1.sol");
			if (solfile.is_open()) {
				int a;
				while(getline(solfile, line)) {
					if (sscanf(line.c_str(), "%i", &a) != 1) {
						return -1;
					}
					suggested_permutation.push_back(a - num_verts_top);
				}
			} else {
				cout << "File couldn't be opened\n";
			}
			cout << "Suggested permutation: ";
			for(int i = 0; i < suggested_permutation.size(); i++) {
				cout << suggested_permutation[i] + num_verts_top << " ";
			}
			cout << "\n";
			cout << "Number crossings in suggested solution: " << calculate_num_crossings(suggested_permutation, crossing_mat) << "\n";
		}
		

		ofstream myfile("solution.sol");
		if (myfile.is_open()) {

			for (auto i : top.ans) {
			    if (i > 0){
				    myfile << i + num_verts_top << "\n";
				}
			}
			myfile.close();
		}
		else cout << "Unable to open file";
	}
	else {
		cout << "Unable to open file";
	}

	return 0;
}