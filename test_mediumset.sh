#!/bin/bash

# Set the directory containing the input files
grDir="./test_sets/medium_test_set/instances"
# Set the directory to store the output files
solDir="./test_solutions_medium"
rightDir="./test_sets/medium_test_set/solutions"
outputFile="output_mediumset.log"
timeFile="time_mediumset.log"
TIMEOUT=3600

format_time() {
    local T=$1
    local H=$(echo $T/3600 | bc)
    local M=$(echo $T/60%60 | bc)
    local S=$(echo $T%60 | bc)

    echo "$H h  $M min  $S sec"  >> "$timeFile"
}

g++ solver.cpp -o solver.out
# Ensure the output directory exists
if [ ! -d "$solDir" ]; then
    mkdir -p "$solDir"
fi

# Clear the output log file before appending new content
: > "$outputFile"
: > "$timeFile"

# Get the current directory where the script and solver.out are located
currentDir=$(pwd)

# Loop through each input file in the input directory
for inputFile in "$grDir"/*; do
    echo $inputFile
    # Check if the input file is indeed a file
    if [ -f "$inputFile" ]; then
        # Run the executable with the input file path
        echo "$inputFile"
        START_TIME=$(date +%s)
        timeout $TIMEOUT "$currentDir/solver.out" "$inputFile"
        EXIT_STATUS=$?
        END_TIME=$(date +%s)
        if [ $EXIT_STATUS -eq 124 ]; then
          echo "$inputFile : " >> "$timeFile"
          echo "Timeout!" >> "$timeFile"
          echo "" >> "$timeFile"
        else
          ELAPSED_TIME=$(($END_TIME - $START_TIME))
          echo "$inputFile : " >> "$timeFile"
          format_time $ELAPSED_TIME
          echo "" >> "$timeFile"
        fi

        # Check if the solution file was created
        solutionFilePath="$currentDir/solution.sol"
        if [ -f "$solutionFilePath" ]; then
            # Get the new name for the solution file
            inputFileName=$(basename "$inputFile")
            newSolutionFileName="${inputFileName%.*}.sol"
            newSolutionFilePath="$solDir/$newSolutionFileName"

            # Move and rename the solution file, overwriting any existing file
            mv -f "$solutionFilePath" "$newSolutionFilePath"
        else
            echo "Lösung für $inputFileName wurde nicht gefunden."
        fi
    else
        echo "Datei $inputFile existiert nicht oder ist keine reguläre Datei."
    fi
done

# Verification

for solFile in "$solDir"/*.sol; do
    # Prüfen, ob eine .sol Datei gefunden wurde
    if [ -f "$solFile" ]; then
        # Extrahiere den Dateinamen ohne Pfad und Erweiterung (.sol)
        solFileName=$(basename "$solFile")
        solFileNameWithoutExt="${solFileName%.sol}"

        # Konstruiere den Pfad zur entsprechenden .gr Datei
        grFilePath="$grDir/$solFileNameWithoutExt.gr"
        rightFilePath="$rightDir/$solFileNameWithoutExt.sol"
        # Überprüfe, ob die .gr Datei existiert
        if [ ! -f "$grFilePath" ]; then
            echo "Warnung: Keine .gr Datei gefunden für $solFile"
            echo "Warnung: Keine .gr Datei gefunden für $solFile" >> "$outputFile"
        else
            echo "Verifiziere $solFile mit $grFilePath ..."
            # Aufrufen der pace2024verifier Funktion mit den entsprechenden Argumenten
            { echo -n "$solFileNameWithoutExt Our Solution     : "; pace2024verifier $grFilePath $solFile; } >> $outputFile 2>&1
            { echo -n "$solFileNameWithoutExt Compared solution: "; pace2024verifier $grFilePath $rightFilePath; echo ""; } >> $outputFile 2>&1
        fi
    fi
done

echo "Verarbeitung abgeschlossen. Ausgaben sind in $outputFile gespeichert."
