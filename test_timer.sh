#!/bin/bash

format_time() {
    local T=$1
    local H=$(echo $T/3600 | bc)
    local M=$(echo $T/60%60 | bc)
    local S=$(echo $T%60 | bc)

    printf "%02d:%02d:%02d\n" $H $M $S
}

g++ solver.cpp -o solver.out

START_TIME=$(date +%s)
./solver.out test_sets/medium_test_set/instances/1.gr
END_TIME=$(date +%s)
printf "Solver Time: \n"
ELAPSED_TIME=$(($END_TIME - $START_TIME))
format_time $ELAPSED_TIME
echo ""
echo -n "Our Solution     : "
pace2024verifier test_sets/medium_test_set/instances/1.gr solution.sol
echo -n "Compared solution: "
pace2024verifier test_sets/medium_test_set/instances/1.gr test_sets/medium_test_set/solutions/1.sol
